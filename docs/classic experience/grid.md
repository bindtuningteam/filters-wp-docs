![grid_settings](../images/classic/08.grid_settings.png)


### Grid Size

Here you can set the Grid Size you want your items to have. You can choose between:

- **Extra Tiny** (100px)
- **Tiny** (150px)
- **Small** (175px)
- **Medium** (200px)
- **Large** (250px)

____
### Tiles Spacing

You can also set the spacing between the tiles. You can choose from:

- Zero 
- Five
- Ten 
- Twenty 

____
### Locked Grid

Activating this option prevents tiles from making small adjustments to their size in order to fill all horizontal space
available.
By default, tiles will always attempt to fill as much horizontal space as possible inside the web part container by adjusting their position and size.