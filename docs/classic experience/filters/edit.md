1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part Filters and click the **Manage Templates** icon;
3. The list of Templates Filters will appear. Click the **Edit** icon to edit the template;
4. You can check what you can edit in each section on the [Template Settings](../../global/template);

	![edit_filter](../../images/classic/03.edit_filter.png)

5. Done editing? Click on **Save Changes** to save your settings.

	![save_changes](../../images/classic/13.save_changes.png)