1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part click the **Manage Templates** icon;
3. The list of Filters Template will appear. Click the **Edit** icon to edit the Template;
4. You can check what you can edit in each section on the [Template Settings](../../global/template);

	![edit-map.gif](../../images/classic/03.edit_filter.png)

5. Done editing? Click on **Save Changes** to save your settings.