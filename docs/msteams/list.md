![list_filters](../images/modern/09.list_filters.png)


### Templates List URL

This is the place to paste the URL to the Templates list. This is where you have all your templates which define how content from the SharePoint list will be presented. Without this value, the web part cannot function and an error message will be displayed requesting that you connect the web part to a templates list.

Paste the URL saved on the step above on the section **Templates List Url**. 

You can create a BindTuning Filters list using the panel, for more information about this, visit the [next link](./createlist).

____
### Data Source URL

Use the **Data Source URL** field to map the data from an external data source, typically a SharePoint List. Paste the URL of  the list that you intend to show information on. Without this value, the web part cannot function and an error message will be displayed requesting that you connect the web part to a source list.

The **Data Source URL** is where your information will be pulled from to display on the Web Part.

___
### Source Query

You can query the list to just show items based on the query built. 

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>

<p class="alert alert-info">It is recommended that you use relative paths in this field. So instead of using a URL like  https://company.sharepoint.com/sites/Home/Lists/Filters you should use something like <b>/sites/Home/Lists/Filters</b>. This will ensure that the web part will work regardless of how you’re accessing the site.</p>