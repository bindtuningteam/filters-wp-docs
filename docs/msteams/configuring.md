1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **Filters**, you can skip this process. 

    ![settings_delete.png](../images/msteams/setting_edit.png)

2. Configure the web part according to the settings described in the **[Web Part Properties](./general.md)**;

    ![07.options.png](../images/modern/07.options.png)
    
3. The properties are saved automatically, so when you're done, simply **Publish** the page and the content will be saved.