1. Open the Team on **Teams panel** that you intend to edit the Filter Template; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Templates** icon;

	![Edit](../../images/msteams/manage_templates.png)

4. The list of Filters Template will appear. Click the **Edit** icon to edit the Template;

	![edit-map.gif](../../images/classic/03.edit_filter.png)

5. You can check what you can edit in each section on the [Template Settings](../../global/template);


6. Done editing? Click on **Save Changes** to save your settings.